<?php



/*require_once('libs/Sms.class.php');
*/

class msg91_sms extends NotificationModule {

    protected $modname = 'Msg91 SMS Notifications';
    protected $description = 'Notify staff members and/or customers about events in HostBill trough msg91 SMS gateway.
        <br> Phone number for staff member can be set in his profile';

    /**
     * Module configuration, visible in Settings->modules
     * @var array
     */
    protected $configuration = array(

        'Authentication key' => array(
            'value' => '',
            'type' => 'input',
            'description' => 'MSG91 Authentication'
        ),
        'Sender name' => array(
            'value' => '',
            'type' => 'input',
            'description' => 'Sender name or phone number.',
        ),

        'Client Field' => array(
            'value' => 'mobilephone',
            'type' => 'input',
            'description' => 'Provide variable name from Clients->Registration fields responsible for holding client mobile phone number.
                If this field is empty no sms notifications will be sent to client trough Msg91'
        )


    );

    /**
     * Install module.
     * We need to add custom admin field for keeping his mobile number
     * We also need add custom client field (it can be later removed / updated by admin)
     */
    public function install() {

        $admin_field = array(
            'name' => 'Mobile phone number',
            'code' => 'mobilephone',
            'type' => 'input'
        );
        $fieldsmanager = HBLoader::LoadModel('EditAdmins/AdminFields');
        $fieldsmanager->addField($admin_field);


        $client_field = array(
            'name' => 'Mobile phone number',
            'code' => 'mobilephone',
            'field_type' => 'input',
            'editable' => true,
            'type' => 'All',
            'description' => 'To receive SMS notifications about your services with us please provide your mobile phone number, starting with country code prefix, ie. +1'
        );
        $clientfieldsmanager = HBLoader::LoadModel('Clients');
        $clientfieldsmanager->addCustomField($client_field);
    }

    /**
     * Send notification to admin.
     * HostBill will automatically execute this function if admin needs
     * to be notified and is allowed to be notified about something
     *
     * @param integer $admin_id Administrator ID to notify (see hb_admin_* tables)
     * @param string $subject Subject (for sms it may be omited)
     * @param string $message Message to send
     */
    public function notifyAdmin($admin_id, $subject, $message) {

        //1. get related admin details, and check if he have mobile phone added
        $editadmins = HBLoader::LoadModel('EditAdmins');
        $admin = $editadmins->getAdminDetails($admin_id);

        if (!$admin) { //admin not found
            return false;
        } elseif (!$admin['mobilephone']) { //admin mobile phone not found
            return false;
        }
        //send message
        return $this->_send($admin['mobilephone'], $message);
    }

    /**
     * Send notification to client
     * HostBill will automatically execute this function if client needs
     * to be notified and is allowed to be notified about something
     *
     *
     * @param integer $client_id Client ID to notify  (see hb_client_* tables)
     * @param string $subject Subject (for sms it may be omitted)
     * @param string $message Message to send
     */
    public function notifyClient($client_id, $subject, $message) {

        $mobile_phone_field = $this->configuration['Client Field']['value'];

        if (!$mobile_phone_field) { //no client field configured->do not notify clients
            return false;
        }

        //. get client details and check for mobile phone field
        $clients = HBLoader::LoadModel('Clients');
        $client_details = $clients->getClient($client_id);

        if (!$client_details) {
            return false;
        } elseif (!$client_details[$mobile_phone_field]) {
            //no mobile phone num provided
            return false;
        }


        //send message
        return $this->_send($client_details[$mobile_phone_field], $message, $client_details['countryname']);
    }


    /**
     * This function is used by SMS-Gateway plugin
     *
     * @param $to To whom sms should be sent
     * @param $from From who sms should be sent
     * @param $text Message text
     *
     * @return boolean
     */
    public function sendClientSMS($to,$from,$text) {
        $this->configuration['Sender name']['value']= $from;
        return $this->_send($to,$text);
    }


    /**
     * Helper function to send actual SMS message to Msg91
     * 
     * @param string $number Phone number
     * @param string $message SMS message to send
     */
    private function _send($number, $message, $country = ''){

        if($country == 'United States')
            $country_id = 1;
        elseif($country == 'India')
            $country_id = 91;
        else
            $country_id = 0;

        $params= [
            'sender'    => $this->configuration['Sender name']['value'],
            'route'     => 4,
            'country'   => $country_id,
            'sms'       => [[
                'message'   => $message,
                'to'        => [$number]
            ]],

        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=$country_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "authkey: $this->configuration['Authentication key'][value]",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        $err = curl_error($curl);

        curl_close($curl);



        if ($err) {
            $this->addError('Api connection error'.$err);
            return false;
        } elseif($response['type']!='success') {
            $this->addError('Api connection error'.$response['message']);
            return false;
        }
        else {
            return true;
        }
    }

}
